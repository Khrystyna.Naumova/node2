import fs from 'fs'
import express from 'express'

const PORT = 8080
const app = express()

app.use(express.json());

// POST
let createFile = (req, res) => {
  let filename = req.body.filename;
  let content = req.body.content;
  if(content === undefined || content === '') {
    res.status(400).send({
      message: `Please specify 'content' parameter`
    });
    return
  } else if (filename === undefined || filename === '') {
    res.status(400).send({
      message: `Please specify 'filename' parameter`
    });
    return
  } else if (!filename.match(/\.(txt|json|yaml|xml|js|log)$/)) {
    res.status(400).send({
      message: `Not supported extention`
    });
    return
  }
  fs.writeFile('test/'.concat(filename), content, err => {
    if(err) {
      res.status(500).send({
        message: `Server error`
      });
      return
    }
    const textResponse = {
      message: 'File created successfully'
    };
    res.json(textResponse);
  })
}

app.post('/api/files', createFile);


// GET
let getFiles = (req, res) => {
  fs.readdir('./test/', (err, items) => {
      if(err) {
        res.status(500).send({
        message: `Server error`
        });
      } 
      let files = []
      items.forEach(file => {
        files.push(file)
      });
      const textResponse = {
        message: 'Success',
        files
      }
      res.json(textResponse);
  });
}

app.get('/api/files', getFiles);


//GET2
let getFile = (req, res) => {
  let pathname = req.params.filename
  fs.readFile('test/'.concat(pathname), 'utf8', (err, data) => {
      if (err) {
        if(err.code === "ENOENT") {
          res.status(400).send({
            message: `No file with ${pathname} filename found`
          });
          return;
        } else {
          res.status(500).send({
            message: `Server error`
          });
          return;
        }
      }
      getUploadedFileResult(pathname, res, data)
  });
}

let getUploadedFileResult = (pathname, res, data) => {
  fs.stat('test/'.concat(pathname), (err, stats) => {
    if (err) throw err;
    try {
      let realUploadedDate = stats.birthtime;
      const textResponse = {
      message: 'Success',
      filename: pathname,
      content: data,
      extension: pathname.split('.').pop(),
      uploadedDate: realUploadedDate
    }
      res.json(textResponse);
    } catch (err) {
      res.status(500).send({
        message: `Server error`
      });
      return;
    }
  })
}

app.get('/api/files/:filename/', getFile);


app.listen(PORT, function() {
	console.log('running...');
});